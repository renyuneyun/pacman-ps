#!/usr/bin/env sh
# Copyright (C) 2017-2019  Rui ZHAO (renyuneyun) <renyuneyun@gmail.com>
# This file and project are licensed under the GPLv2 or greater at your choice.
# For more information view the license included or visit:
# https://www.gnu.org/licenses/gpl-2.0.html
#
# This script is meant to be executed by the pacman-ps-{pre,post}.hook.
# It cleans up DB_DIR/files.db to keep needed entries only.
DB_DIR="/var/cache/pacman-ps"
pacman-ps -sq | awk '{ print $2 " " $1 }' | sort | uniq >> "${DB_DIR}/files.db.clean"
mv "${DB_DIR}/files.db.clean" "${DB_DIR}/files.db"

