#!/usr/bin/env sh
# Copyright (C) 2017-2019  Rui ZHAO (renyuneyun) <renyuneyun@gmail.com>
# This file and project are licensed under the GPLv2 or greater at your choice.
# For more information view the license included or visit:
# https://www.gnu.org/licenses/gpl-2.0.html
#
# This script is meant to be executed by the pacman-ps-{pre-post}.hook.
# It receives a list of package names seperated by a newline from stdin.
# It then will add the file listing to the DB_DIR/files.db.
# Finally it performs database optimisation (pacman-ps-optimise-db.sh).

DB_DIR="/var/cache/pacman-ps"

xargs -I '{}' pacman -Ql '{}' >> "${DB_DIR}/files.db"

